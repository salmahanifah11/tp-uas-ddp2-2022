public class Dosen extends Manusia{
    private String nip;
    private Matakuliah[] daftarMatkul;
    private int jumlahMatkul;

    public Dosen(String newNama, String newNip) {
        super(newNama);
        this.nip = newNip;
    }

    public String getNama() {
        return this.nama;
    }
    public String getNip() {
        return this.nip;
    }

    public void assignMatkul(Matakuliah matkul) {
        if (jumlahMatkul == 0) {
            Matakuliah[] counter = new Matakuliah[1];
            counter[0] = matkul;
            daftarMatkul= counter;
            jumlahMatkul++;
            matkul.assignDosen(this);
        } else {
            if (jumlahMatkul == daftarMatkul.length) {
                Matakuliah[] counter = new Matakuliah[daftarMatkul.length*2];
                System.arraycopy(daftarMatkul, 0, counter, 0, daftarMatkul.length);
                daftarMatkul = counter;
            }
        }
    }

    public Matakuliah[] getDaftarMatkul() {
        return this.daftarMatkul;
    }
}
