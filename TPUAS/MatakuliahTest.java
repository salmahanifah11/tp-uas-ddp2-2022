import static org.junit.Assert.*;
import org.junit.Test;
import java.util.*;                                                                                                                                                                                                                     

public class MatakuliahTest {
    @Test
    public void testKuliah() {
        Mahasiswa mhs1 = new Mahasiswa("Burhan", "129500004Y");
        Mahasiswa mhs2 = new Mahasiswa("Bubur","1299123456");
        Matakuliah kul1 = new Matakuliah("DDP2","CSGE601021");

        kul1.tambahMhs(mhs1);
        kul1.tambahMhs(mhs2);
        mhs1.tambahMatkul(kul1);
        Mahasiswa[] daftarMhs = kul1.getDaftarMhs();
        List<Mahasiswa> arr = Arrays.asList(daftarMhs);

        assertEquals(kul1.getNamaMatkul(), "DDP2");
        assertEquals(kul1.getKode(), "CSGE601021");
        assertTrue(arr.contains(mhs1));
        assertEquals("CSGE601021", mhs1.getDaftarMatkul()[0].getKode());
    }

    @Test
    public void testDosen() {
        Dosen dosen1 = new Dosen("Hafizh", "012345678");
        Matakuliah kul2 = new Matakuliah("DDP2", "CSGE601021");
        dosen1.assignMatkul(kul2);
        kul2.assignDosen(dosen1);

        assertEquals("Hafizh", kul2.getDosen().getNama());
    }
}
