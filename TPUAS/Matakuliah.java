public class Matakuliah {
    private String namaMatkul;
    private String kode;
    private int jumlahMhs;
    private Mahasiswa[] daftarMhs;
    private Dosen dosen;

    public Matakuliah(String newNamaMatkul, String newKode) {
        this.namaMatkul = newNamaMatkul;
        this.kode = newKode;
    }
    
    public void setKode(String kode) {
        this.kode = kode;
    }
    public String getNamaMatkul() {
        return this.namaMatkul;
    }
    public String getKode() {
        return this.kode;
    }
    public int getJumlahMhs() {
        return this.jumlahMhs;
    }

    public void tambahMhs(Mahasiswa mhs) {
        if (jumlahMhs ==0) {
            Mahasiswa[] counter = new Mahasiswa[1];
            counter[0] = mhs;
            daftarMhs = counter;
            jumlahMhs++;
        } else {
            if (jumlahMhs == daftarMhs.length) {
                Mahasiswa[] counter = new Mahasiswa[daftarMhs.length];
                System.arraycopy(daftarMhs, 0, counter, 0, daftarMhs.length);
                daftarMhs = counter;
            }
        }
    }
    public void dropMhs(Mahasiswa mhs) {
        if (daftarMhs.length > 0) {
            for (int i=0; i<jumlahMhs; i++) {
                if (daftarMhs[i].getNama().equals(mhs.getNama())) {
                    for (int j=i+1; j<jumlahMhs; j++) {
                        daftarMhs[j-1] = daftarMhs[j];
                    }
                    jumlahMhs--;
                    break;
                }
            }
        }
    }
    public String toString() {
        return this.namaMatkul + " " + this.kode + " " + this.jumlahMhs;
    }
    public void cetakMatkul() {
        System.out.println("Mata Kuliah " + this.namaMatkul);
        System.out.println("Kode " + this.kode);
        System.out.println("Matakuliah");
        for ( int i=0; i<jumlahMhs; i++) {
            System.out.println(daftarMhs[i].getNama() + daftarMhs[i].getNpm());
        }
    }
    public void assignDosen(Dosen newDosen) {
        this.dosen = newDosen;
    }

    public Mahasiswa[] getDaftarMhs() {
        return daftarMhs;
    }
    public Dosen getDosen() {
        return this.dosen;
    }
}
