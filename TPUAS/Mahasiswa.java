import java.util.*;

public class Mahasiswa extends Manusia{
    private String npm;
    private int jumlahMatkul;
    private Matakuliah[] daftarMatkul;
    private HashMap<String, String> kodeMatkul = new HashMap<String, String>();
    
    public Mahasiswa(String newNama, String newNpm) {
        super(newNama);
        this.npm = newNpm;
        kodeMatkul();
    }

    public String getNama() {
        return this.nama;
    }
    public String getNpm() {
        return this.npm;
    }
    public int getJumlahMatkul() {
        return this.jumlahMatkul;
    }

    private void kodeMatkul() {
        kodeMatkul.put("UIGE", "Mata Kuliah Wajib Universitas");
        kodeMatkul.put("UIST", "Mata Kuliah Wajib Rumpun Sains dan Teknologi");
        kodeMatkul.put("CSGE", "Mata Kuliah Wajib Fakultas");
        kodeMatkul.put("CSCM", "Mata Kuliah Wajib Program Studi Ilmu Komputer");
        kodeMatkul.put("CSIM", "Mata Kuliah Wajib Program Studi Sistem Informasi");
        kodeMatkul.put("CSCE", "Mata Kuliah Peminatan Program Studi Ilmu Komputer");
        kodeMatkul.put("CSIE", "Mata Kuliah Peminatan Program Studi Sistem Informasi");
    }
    public void tambahMatkul(Matakuliah matkul) {
        if (jumlahMatkul == 0) {
            Matakuliah[] counter = new Matakuliah[1];
            counter[0] = matkul;
            daftarMatkul = counter;
            matkul.tambahMhs(this);
            jumlahMatkul++;
        } else {
            if (jumlahMatkul == daftarMatkul.length) {
                Matakuliah[] counter = new Matakuliah[daftarMatkul.length*2];
                System.arraycopy(daftarMatkul, 0, counter, 0, daftarMatkul.length);
                daftarMatkul = counter;
            }
        }
    }
    public void dropMatkul(Matakuliah matkul) {
        if (daftarMatkul.length > 0) {
            for (int i=0; i<jumlahMatkul; i++) {
                if (daftarMatkul[i].getNamaMatkul().equals(matkul.getNamaMatkul())) {
                    for (int j= i+1; j<jumlahMatkul; j++) {
                        daftarMatkul[j-1] = daftarMatkul[j];
                    }
                    jumlahMatkul--;
                    break;
                }
            }
        }
    }
    public String toString() {
        return this.nama + " " + this.npm + " " + this.jumlahMatkul;
    }
    public void cetakMhs() {
        System.out.println("Mahasiswa " + this.nama);
        System.out.println("NPM " + this.npm);
        System.out.println("Matakuliah");
        for ( int i=0; i<jumlahMatkul; i++) {
            System.out.println(daftarMatkul[i].getNamaMatkul() + kodeMatkul.get(daftarMatkul[i].getKode()));
        }
    }

    public Matakuliah[] getDaftarMatkul() {
        return daftarMatkul;
    }
}